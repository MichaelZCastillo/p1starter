package edu.uprm.ece.icom4035.list;

import java.util.Iterator;

public class ArrayList<E> implements List<E> {

	private static final int INITCAP = 1; 
	private static final int CAPTOAR = 1; 
	private static final int MAXEMPTYPOS = 2; 
	private E[] element; 
	private int size;
	private int capacity;
	
	public ArrayList() { 
		element = (E[]) new Object[INITCAP]; 
		size = 0; 
	} 
	@Override
	public Iterator<E> iterator() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void add(E e) {
		// TODO Auto-generated method stub
		if(size >= element.length) {
			changeCapacity(CAPTOAR);
		}
		element[size] = e;
			size++;
	}

	@Override
	public void add(int index, E e) throws IndexOutOfBoundsException {
		// ADD CODE AS REQUESTED BY EXERCISES
		if(index < 0 || index> size) {
			throw new IndexOutOfBoundsException("add: Invalid index = " + index);
		}
		if(size >= element.length) {
			changeCapacity(CAPTOAR);
		}
			moveDataOnePositionTR(index ,size - 1);
		element[index] = e;
				size++;
	}

	@Override
	public boolean remove(E obj) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean remove(int index) throws IndexOutOfBoundsException {
		return false;
	}

	@Override
	public int removeAll(E obj) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public E get(int index) throws IndexOutOfBoundsException {
		// ADD AND MODIGY CODE AS REQUESTED BY EXERCISES
		if(index <0 || index > size) {
			throw new IndexOutOfBoundsException("get: Invalid index = " + index);
		}
		
		return element[index]; 
	}

	@Override
	public E set(int index, E e) throws IndexOutOfBoundsException {
		// ADD AND MODIFY CODE AS REQUESTED BY EXERCISES
		if(index <0 || index > size) {
			throw new IndexOutOfBoundsException("set: Invalid index = " + index);
		}
		E replaced = element[index];
		element[index] = e;
		
		return replaced;
	}

	@Override
	public E first() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public E last() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int firstIndex(E obj) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int lastIndex(E obj) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public boolean isEmpty() {
		return size == 0;
	}

	@Override
	public boolean contains(E obj) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void clear() {
		// TODO Auto-generated method stub
		
	}

	public int capacity() {
		return element.length;
	}
	
	private void changeCapacity(int change) { 
		int newCapacity = element.length + change; 
		E[] newElement = (E[]) new Object[newCapacity]; 
		for (int i=0; i<size; i++) { 
			newElement[i] = element[i]; 
			element[i] = null; 
		} 
		element = newElement; 
	}
	
	private void shrinkCapacity(int change) { 
		int newCapacity = element.length - change; 
		E[] newElement = (E[]) new Object[newCapacity]; 
		for (int i=0; i<size; i++) { 
			newElement[i] = element[i]; 
			element[i] = null; 
		} 
		element = newElement; 
	}
	
	// useful when adding a new element with the add
	// with two parameters....
	private void moveDataOnePositionTR(int low, int sup) { 
		// pre: 0 <= low <= sup < (element.length - 1)
		for (int pos = sup; pos >= low; pos--)
			element[pos+1] = element[pos]; 
	}

	// useful when removing an element from the list...
	private void moveDataOnePositionTL(int low, int sup) { 
		// pre: 0 < low <= sup <= (element.length - 1)
		for (int pos = low; pos <= sup; pos++)
			element[pos-1] = element[pos]; 
	}

}
