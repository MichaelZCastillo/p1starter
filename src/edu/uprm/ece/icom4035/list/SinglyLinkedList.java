package edu.uprm.ece.icom4035.list;

import java.util.Iterator;
import java.util.NoSuchElementException;

import edu.uprm.ece.icom4035.list.Node;

public class SinglyLinkedList<E> implements List<E> {

	private SNode<E> first; 
	private int length;
	private int size;

	private class ElementsIterator implements Iterator<E> { // INNER to ArrayIndexList
		private Node<E> curr = first; // node containing element to return on next next()
		private Node<E> ptntr = null; // node preceding node valid to be removed
		private boolean canRemove = false; // to control when remove() is valid to execute
		public boolean hasNext() { return curr != null; }
		public E next() {
			if (!hasNext()) throw new NoSuchElementException("Iterator is completed.");
			if (canRemove) ptntr = (ptntr == null ? first : ((SNode<E>) ptntr).getNext()); // Why this? Think...
			canRemove = true;
			E etr = curr.getElement(); curr = ((SNode<E>) curr).getNext(); // get element and prepare for future
			return etr;
		}

		public void remove() {
			if (!canRemove) throw new IllegalStateException("Not valid to remove.");
			if (ptntr == null) first = first.getNext(); // removes the first node
			else ((SinglyLinkedList<E>.SNode<E>) ptntr).setNext(((SinglyLinkedList<E>.SNode<E>) ptntr).getNext().getNext()); // removes node after ptntr
			size--; canRemove = false;
		}
	}

	public void add(E obj) {
		// TODO Auto-generated method stub
		ElementsIterator iter = new ElementsIterator();
		while (iter.hasNext()) {
			iter.next();
	}

	public void add(int index, E obj) {
		// TODO Auto-generated method stub

	}

	public boolean remove(E obj) {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean remove(int index) {
		// TODO Auto-generated method stub
		return false;
	}

	public int removeAll(E obj) {
		// TODO Auto-generated method stub
		return 0;
	}

	public E get(int index) {
		// TODO Auto-generated method stub
		return null;
	}

	public E set(int index, E obj) {
		// TODO Auto-generated method stub
		return null;
	}

	public E first() {
		// TODO Auto-generated method stub
		return null;
	}

	public E last() {
		// TODO Auto-generated method stub
		return null;
	}

	public int firstIndex(E obj) {
		// TODO Auto-generated method stub
		return 0;
	}

	public int lastIndex(E obj) {
		// TODO Auto-generated method stub
		return 0;
	}

	public int size() {
		// TODO Auto-generated method stub
		return length; 
	}

	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return length == 0;
	}

	public boolean contains(E obj) {
		// TODO Auto-generated method stub
		return false;
	}

	public void clear() {
		// TODO Auto-generated method stub

	}

	public class SNode<E> implements Node<E> {
		private E element; 
		private SNode<E> next; 
		public SNode() { 
			element = null; 
			next = null; 
		}
		public SNode(E data, SNode<E> next) { 
			this.element = data; 
			this.next = next; 
		}
		public SNode(E data)  { 
			this.element = data; 
			next = null; 
		}
		public E getElement() {
			return element;
		}
		public void setElement(E data) {
			this.element = data;
		}
		public SNode<E> getNext() {
			return next;
		}
		public void setNext(SNode<E> next) {
			this.next = next;
		}
	}
}
